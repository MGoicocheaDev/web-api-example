﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http.ModelBinding;
using web_api_sample.Models;

namespace web_api_sample.Utilities
{
    public class Funciones
    {

        public static List<ErrorModelState> ObtenerErrores(ModelStateDictionary modelState )
        {
            var errorModel = modelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();

            List<ErrorModelState> listaResultado = new List<ErrorModelState>();
            foreach (var error in errorModel)
            {
                //string[] parts = error.Key.Split('.');

                string item = Regex.Match(error.Key, @"\d+").Value;
                int esNuevo = 1;
                foreach (var obj in listaResultado) {
                    if (obj.Item.ToString() == item.ToString())
                    {
                        esNuevo = 0;
                        break;
                    }
                    else {
                        esNuevo = 1;
                    }
                }
                var DetailErrors = new List<DetailError>();
                DetailErrors.AddRange(error.Errors.Select(x => new DetailError { ErrorNumber = 1, Description = x.ErrorMessage.ToString() }));

                if (esNuevo == 1)
                {
                    listaResultado.Add(new ErrorModelState {Item = item, ListaErrores = DetailErrors});
                }
                else {
                    listaResultado.Where(x => x.Item == item.ToString()).First().ListaErrores.AddRange(DetailErrors);
                }
            }

            return listaResultado;
        }
    }
}