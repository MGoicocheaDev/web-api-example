﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.Results;
using web_api_sample.Models;

namespace web_api_sample.Handler
{
    /// <summary>
    /// Valid model attributes
    /// </summary>
    public class ModelStateValidator : ActionFilterAttribute
    {
        /// <summary>
        /// override action
        /// valid model state
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.ModelState.IsValid == false)
            {
                var errorModel =
                        from x in actionContext.ModelState.Keys
                        where actionContext.ModelState[x].Errors.Count > 0
                        select new
                        {
                            key = x,
                            errors = actionContext.ModelState[x].Errors.
                                                          Select(y => y.ErrorMessage).
                                                          ToArray()
                        };


                var DetailErrors = new List<DetailError>();

                foreach(var error in errorModel)
                {
                    DetailErrors.AddRange(error.errors.Select(x => new DetailError { ErrorNumber = 1, Description = x.ToString() }));
                }


                var response = new GenericResponse<string>
                {
                    Data = null,
                    ErrorManager = new ErrorManager
                    {
                        Status = 400,
                        Errors = DetailErrors
                    }
                };

                actionContext.Response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(JsonConvert.SerializeObject(response), Encoding.UTF8, "application/json")
                };
            }
        }
    }
}