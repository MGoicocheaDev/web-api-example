﻿using System;
using System.ComponentModel.DataAnnotations;
using web_api_sample.Models;

namespace web_api_sample.CustomValidations
{
    /// <summary>
    /// Custom validation for size company
    /// </summary>
    public class SizeCompanyValidation : ValidationAttribute
    {
        /// <summary>
        /// Override event IsValid
        /// </summary>
        /// <param name="value"></param>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            bool valid;

            valid = Enum.IsDefined(typeof(SizeCompany), value);

            if (valid)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            }            
        }
    }
}