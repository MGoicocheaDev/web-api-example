﻿using System.ComponentModel.DataAnnotations;

namespace web_api_sample.CustomValidations
{
    /// <summary>
    /// Custom Validation for type document 
    /// </summary>
    public class ContactTypeDocumentValidaton : ValidationAttribute
    {
        /// <summary>
        /// Override event IsValid
        /// </summary>
        /// <param name="value"></param>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if(value.ToString().ToUpper() != "DNI" && value.ToString().ToUpper() != "CE")
            {
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            }

            return ValidationResult.Success;
        }
    }
}