using Swashbuckle.Application;
using Swashbuckle.Examples;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Description;


namespace web_api_sample
{
    public class SwaggerConfig
    {
        public static void Register(HttpConfiguration config)
        {

            var apiExplorer = config.AddVersionedApiExplorer(o => o.GroupNameFormat = "'v'V");

            config
                .EnableSwagger(c =>
                {
                    c.PrettyPrint();
                    c.MultipleApiVersions(
                       (apiDesc, targetApiVersion) => apiDesc.GetGroupName() == targetApiVersion,
                       (vc) =>
                       {
                           vc.Version("v1", $"Web Api Sample v1");
                       });

                    c.SchemaId(x => x.FullName);
                    c.ResolveConflictingActions(x => x.First());
                    c.IncludeXmlComments(GetXmlCommentsPath());
                    c.OperationFilter<ExamplesOperationFilter>();
                })
            .EnableSwaggerUi(c => {
                c.EnableDiscoveryUrlSelector();
            });
        }

        /// <summary>
        /// Get XML Comments
        /// </summary>
        /// <returns></returns>
        private static string GetXmlCommentsPath()
        {
            return Path.Combine(System.Web.HttpRuntime.AppDomainAppPath, "bin", string.Format("{0}.xml", Assembly.GetCallingAssembly().GetName().Name));
        }

    }
}
