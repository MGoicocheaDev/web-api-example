﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_api_sample.Models
{
    public class ErrorManager
    {
        /// <summary>
        /// status application
        /// 200 => OK
        /// 400 => Error
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// List of errors
        /// </summary>
        public List<DetailError> Errors { get; set; }

    }

    public class DetailError
    {
        /// <summary>
        /// code error
        /// </summary>
        public int ErrorNumber { get; set; }

        /// <summary>
        /// description error
        /// </summary>
        public string Description { get; set; }
    }

    public class ErrorModelState{
        public string Item { get; set; }
        public List<DetailError> ListaErrores { get; set; }
    }
}