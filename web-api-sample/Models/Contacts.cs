﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using web_api_sample.CustomValidations;
using web_api_sample.Resource;

namespace web_api_sample.Models
{
    /// <summary>
    /// DTO CONTACT
    /// </summary>
    public class ContactBase
    {
        /// <summary>
        /// Tipo de documento
        /// DNI / CE
        /// </summary>
        [JsonProperty(Order = 0)]
        [Display(Name = "contact_typedocument", ResourceType = typeof(literals))]
        [Required(ErrorMessageResourceName = "contact_typedocument_required", ErrorMessageResourceType = typeof(literals))]
        [ContactTypeDocumentValidaton(ErrorMessageResourceName = "contact_typedocument_invalid", ErrorMessageResourceType = typeof(literals))]
        public string TypeDocument { get; set; }

        /// <summary>
        /// Document Number of contact
        /// </summary>
        [JsonProperty(Order = 1)]
        [Display(Name = "contact_documentnumber", ResourceType = typeof(literals))]
        [Required(ErrorMessageResourceName = "contact_documentnumber_required", ErrorMessageResourceType = typeof(literals))]
        public string DocumentNumber { get; set; }

        /// <summary>
        /// First Name of contact
        /// </summary>
        [JsonProperty(Order = 2)]
        [Display(Name = "contact_firstname", ResourceType = typeof(literals))]
        [Required(ErrorMessageResourceName = "contact_firstname_required", ErrorMessageResourceType = typeof(literals))]
        public string FirstName { get; set; }

        /// <summary>
        /// Last Name of contact
        /// </summary>
        [JsonProperty(Order = 3)]
        [Display(Name = "contact_lastname", ResourceType = typeof(literals))]
        [Required(ErrorMessageResourceName = "contact_lastname_required", ErrorMessageResourceType = typeof(literals))]
        public string LastName { get; set; }

        /// <summary>
        /// Phone Number of contact
        /// </summary>
        [JsonProperty(Order = 4)]
        [Display(Name = "contact_phone", ResourceType = typeof(literals))]
        [Required(ErrorMessageResourceName = "contact_phone_required", ErrorMessageResourceType = typeof(literals))]
        [Phone(ErrorMessageResourceName = "contact_phone_format", ErrorMessageResourceType = typeof(literals))]
        public string Phone { get; set; }

        /// <summary>
        /// Email of contact
        /// </summary>
        [JsonProperty(Order = 5)]
        [Display(Name = "contact_email", ResourceType = typeof(literals))]
        [Required(ErrorMessageResourceName = "contact_email_required", ErrorMessageResourceType = typeof(literals))]
        [EmailAddress(ErrorMessageResourceName = "contact_email_format", ErrorMessageResourceType = typeof(literals))]
        public string Email { get; set; }
    }

    /// <summary>
    /// Contact
    /// </summary>
    public class Contact : ContactBase
    {
        /// <summary>
        /// Contact ID
        /// </summary>
        [JsonProperty(Order = -1)]
        [Display(Name = "contact_id", ResourceType = typeof(literals))]
        public int IdContact { get; set; }
    }
}