﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_api_sample.Models
{
    public class GenericResponse<T>
    {
        /// <summary>
        /// Data 
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// Error Manager
        /// </summary>
        public ErrorManager ErrorManager { get; set; }
    }

    public class GenericListResponse<T>
    {
        /// <summary>
        /// Data 
        /// </summary>
        public List<T> Data { get; set; }

        /// <summary>
        /// Error Manager
        /// </summary>
        public ErrorManager ErrorManager { get; set; }
    }
}