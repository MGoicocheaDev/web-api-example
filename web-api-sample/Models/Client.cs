﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using web_api_sample.CustomValidations;
using web_api_sample.Resource;

namespace web_api_sample.Models
{
    /// <summary>
    /// enum options for size business
    /// </summary>
    public enum SizeCompany
    {
        /// <summary>
        /// small company
        /// 0 - 100 employes
        /// </summary>
        small = 100,
        /// <summary>
        /// medium company
        /// 101 - 500 employes
        /// </summary>
        medium = 200,
        /// <summary>
        /// big company
        /// 500 - more employes
        /// </summary>
        big = 300

    }

    /// <summary>
    /// DTO CLIENT Base
    /// </summary>
    public class ClientBase
    {
        /// <summary>
        /// Document Number
        /// Peru => RUC
        /// </summary>
        [JsonProperty(Order = 0)]
        [Display(Name ="company_number",ResourceType = typeof(literals))]
        [Required(ErrorMessageResourceName = "company_number_required", ErrorMessageResourceType = typeof(literals))]
        public string CompanyNumber { get; set; }

        /// <summary>
        /// Name of Client
        /// </summary>        
        [JsonProperty(Order = 1)]
        [Display(Name = "company_name", ResourceType = typeof(literals))]
        [Required(ErrorMessageResourceName = "company_name_required", ErrorMessageResourceType = typeof(literals))]
        public string Name { get; set; }

        /// <summary>
        /// Size Business
        /// </summary>        
        [JsonProperty(Order = 2)]
        [Display(Name = "company_size", ResourceType = typeof(literals))]
        [Required(ErrorMessageResourceName = "company_size_required", ErrorMessageResourceType = typeof(literals))]
        [SizeCompanyValidation(ErrorMessageResourceName = "company_size_validation", ErrorMessageResourceType =(typeof(literals)))]
        public SizeCompany Size { get; set; }


    }

    /// <summary>
    /// Client full
    /// </summary>
    public class Client : ClientBase
    {
        /// <summary>
        /// Client Id
        /// </summary>
        [JsonProperty(Order = -1)]
        [Display(Name = "company_id", ResourceType = typeof(literals))]
        public int ClientId { get; set; }

        /// <summary>
        /// List of Contacts
        /// </summary>
        [JsonProperty(Order = 3)]
        [Display(Name = "company_contacts", ResourceType = typeof(literals))]
        public List<Contact> Contacts { get; set; }

        public static implicit operator Client(List<ClientRespuesta> v)
        {
            throw new NotImplementedException();
        }
    }

    public class ClientRespuesta {
        public Boolean isCorrect { get; set; }
        public string codigoRecargo { get; set; }
        public string codigoRecargoIntegracion { get; set; }
        public List<DetailError> Errors { get; set; }
    }



}