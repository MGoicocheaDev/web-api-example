﻿using Microsoft.Web.Http;
using Swashbuckle.Examples;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using web_api_sample.CustomSwagger.ResponseExample;
using web_api_sample.Handler;
using web_api_sample.Models;
using web_api_sample.Utilities;

namespace web_api_sample.Controllers
{
    /// <summary>
    /// Client controller
    /// </summary>
    [ApiVersion("1")]
    public class ClientController : ApiController
    {

        #region Client

        /// <summary>
        /// List of clients
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/v{version:apiVersion}/client")]
        [ResponseType(typeof(GenericListResponse<Client>))]
        public IHttpActionResult Get()
        {
            var listCompanies = new List<Client>();
            for (int i = 0; i < 10; i++)
            {

                var faker = new Bogus.Faker("es");

                var client = new Client
                {
                    ClientId = faker.Random.Number(1, 9999),
                    CompanyNumber = faker.PickRandom<string>(BaseApp.Base.GetListRuc()),
                    Name = faker.Company.CompanyName(),
                    Size = faker.PickRandom<SizeCompany>(),
                    Contacts = new List<Contact>()
                };

                int count = faker.Random.Number(1, 5);

                for (int j = 0; j < count; j++)
                {
                    var faker_contact = new Bogus.Faker("es");

                    client.Contacts.Add(new Contact
                    {
                        IdContact = faker_contact.Random.Number(1, 9999),
                        DocumentNumber = Bogus.Extensions.UnitedStates.ExtensionsForUnitedStates.Ssn(new Bogus.Person("es")).ToString(),
                        FirstName = faker_contact.Person.FirstName,
                        LastName = faker_contact.Person.LastName,
                        Email = faker_contact.Person.Email,
                        Phone = faker_contact.Person.Phone,
                        TypeDocument = faker_contact.PickRandom<string>(BaseApp.Base.GetListTypeDocumentsContact())
                    });
                }

                listCompanies.Add(client);
            }



            return Ok(new GenericListResponse<Client> {
                Data = listCompanies,
                ErrorManager = new ErrorManager
                {
                    Status = 200,
                    Errors = new List<DetailError>
                    {
                        new DetailError
                        {
                            ErrorNumber = 0,
                            Description = "Exito"
                        }
                    }
                }
            });
        }

        /// <summary>
        /// Get specific Client
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/v{version:apiVersion}/client/{id}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(GenericResponse<Client>))]
        //[SwaggerResponseExample(HttpStatusCode.OK, typeof(ClientResponseExamples))]
        //[SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(GenericResponse<string>))]
        //[SwaggerResponseExample(HttpStatusCode.BadRequest, typeof(ClientResponseErrorExamples))]
        public IHttpActionResult Get(int id)
        {
            var faker = new Bogus.Faker("es");

            var client = new Client
            {
                ClientId = id,
                CompanyNumber = faker.PickRandom<string>(BaseApp.Base.GetListRuc()),
                Name = faker.Company.CompanyName(),
                Size = faker.PickRandom<SizeCompany>(),
                Contacts = new List<Contact>()
            };

            int count = faker.Random.Number(1, 5);

            for (int j = 0; j < count; j++)
            {
                var faker_contact = new Bogus.Faker("es");

                client.Contacts.Add(new Contact
                {
                    IdContact = faker_contact.Random.Number(1, 9999),
                    DocumentNumber = Bogus.Extensions.UnitedStates.ExtensionsForUnitedStates.Ssn(new Bogus.Person("es")).ToString(),
                    FirstName = faker_contact.Person.FirstName,
                    LastName = faker_contact.Person.LastName,
                    Email = faker_contact.Person.Email,
                    Phone = faker_contact.Person.Phone,
                    TypeDocument = faker_contact.PickRandom<string>(BaseApp.Base.GetListTypeDocumentsContact())
                });
            }



            return Ok(new GenericResponse<Client>
            {
                Data = client,
                ErrorManager = new ErrorManager
                {
                    Status = 200,
                    Errors = new List<DetailError>
                    {
                        new DetailError
                        {
                            ErrorNumber = 0,
                            Description = "Exito"
                        }
                    }
                }
            });
        }

        /// <summary>
        /// Create Client
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v{version:apiVersion}/client")]
        [ResponseType(typeof(GenericResponse<string>))]
        [ModelStateValidator]
        public IHttpActionResult Post(ClientBase client)
        {
            return Ok();
        }

        /// <summary>
        /// Create Client
        /// </summary>
        /// <param name="clientList"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v{version:apiVersion}/clientPostList")]
        [ResponseType(typeof(GenericResponse<string>))]
        //[ModelStateValidator]
        public IHttpActionResult PostList(List<ClientBase> clientList)
        {
            List<ClientRespuesta> listaRespuesta = new List<ClientRespuesta>();
            List<ErrorModelState> listaError = Funciones.ObtenerErrores(ModelState);
            for (int i = 0; i < clientList.Count(); i++)
            {
                ClientBase clientBase = clientList[i];
                ClientRespuesta obj = new ClientRespuesta();
                obj.Errors = new List<DetailError>();
                obj.codigoRecargoIntegracion = clientBase.Size.ToString();
                //obj.Errors = listaError[i].ListaErrores.Count() > 0 ? listaError[i].ListaErrores : new List<DetailError>() { new DetailError { ErrorNumber = 0, Description = "Exito" } };

                if (listaError.Where(x => x.Item == i.ToString()).Any())
                {
                    obj.isCorrect = false;
                    obj.codigoRecargo = "";
                    obj.Errors = listaError.Where(x => x.Item == i.ToString()).First().ListaErrores;
                }
                else
                {
                    //Validaciones funcionales
                    //Registro BD
                    obj.isCorrect = true;
                    Random rnd = new Random();
                    obj.codigoRecargo = rnd.Next().ToString();
                }
                listaRespuesta.Add(obj);
            }

            return Ok(new GenericResponse<List<ClientRespuesta>>
            {
                Data = listaRespuesta,
                ErrorManager = new ErrorManager
                {
                    Status = 200,
                    Errors = new List<DetailError>
                    {
                        new DetailError
                        {
                            ErrorNumber = 0,
                            Description = "Exito"
                        }
                    }
                }
            });
        }

        /// <summary>
        /// Update client by Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="client"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("api/v{version:apiVersion}/client/{id}")]
        [ResponseType(typeof(GenericResponse<string>))]
        public IHttpActionResult Put(int id, ClientBase client)
        {
            return Ok();
        }

        /// <summary>
        /// Delete client by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/v{version:apiVersion}/client/{id}")]
        [ResponseType(typeof(GenericResponse<string>))]
        public IHttpActionResult Delete(int id)
        {
            return Ok();
        }
        #endregion

        

    }
}
