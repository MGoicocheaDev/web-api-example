﻿using Microsoft.Web.Http;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using web_api_sample.Handler;
using web_api_sample.Models;

namespace web_api_sample.Controllers
{
    /// <summary>
    /// Contact controller
    /// </summary>
    [ApiVersion("1")]
    public class ContactController : ApiController
    {
        #region Contact
        /// <summary>
        /// Get list contacts of client
        /// </summary>
        /// <param name="idClient"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/v{version:apiVersion}/client/{idClient}/contact")]
        [ResponseType(typeof(GenericListResponse<Contact>))]
        public IHttpActionResult GetContact(int idClient)
        {
            var faker = new Bogus.Faker("es");
            int count = faker.Random.Number(1, 5);

            var contacts = new List<Contact>();
            for (int j = 0; j < count; j++)
            {
                var faker_contact = new Bogus.Faker("es");

                contacts.Add(new Contact
                {
                    IdContact = faker_contact.Random.Number(1, 9999),
                    DocumentNumber = Bogus.Extensions.UnitedStates.ExtensionsForUnitedStates.Ssn(new Bogus.Person("es")).ToString(),
                    FirstName = faker_contact.Person.FirstName,
                    LastName = faker_contact.Person.LastName,
                    Email = faker_contact.Person.Email,
                    Phone = faker_contact.Person.Phone,
                    TypeDocument = faker_contact.PickRandom<string>(BaseApp.Base.GetListTypeDocumentsContact())
                });
            }


            return Ok(new GenericListResponse<Contact>
            {
                Data = contacts,
                ErrorManager = new ErrorManager
                {
                    Status = 200,
                    Errors = new List<DetailError>
                    {
                        new DetailError
                        {
                            ErrorNumber = 0,
                            Description = "Exito"
                        }
                    }
                }
            });
        }

        /// <summary>
        /// Get specific contact for specific client
        /// </summary>
        /// <param name="idClient"></param>
        /// <param name="idContact"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/v{version:apiVersion}/client/{idClient}/contact/{idContact}")]
        [ResponseType(typeof(GenericResponse<Contact>))]
        public IHttpActionResult GetContact(int idClient, int idContact)
        {
            var faker = new Bogus.Faker("es");

            var contact = new Contact
            {
                IdContact = idContact,
                DocumentNumber = Bogus.Extensions.UnitedStates.ExtensionsForUnitedStates.Ssn(new Bogus.Person("es")).ToString(),
                FirstName = faker.Person.FirstName,
                LastName = faker.Person.LastName,
                Email = faker.Person.Email,
                Phone = faker.Person.Phone,
                TypeDocument = faker.PickRandom<string>(BaseApp.Base.GetListTypeDocumentsContact())
            };

            return Ok(new GenericResponse<Contact>
            {
                Data = contact,
                ErrorManager = new ErrorManager
                {
                    Status = 200,
                    Errors = new List<DetailError>
                    {
                        new DetailError
                        {
                            ErrorNumber = 0,
                            Description = "Exito"
                        }
                    }
                }
            });
        }

        /// <summary>
        /// Create contact for specific client
        /// </summary>
        /// <param name="idClient"></param>
        /// <param name="client"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v{version:apiVersion}/client/{idClient}/contact")]
        [ResponseType(typeof(GenericResponse<string>))]
        [ModelStateValidator]
        public IHttpActionResult PostContact(int idClient, ContactBase client)
        {
            return Ok();
        }

        /// <summary>
        /// Update contact for specific client
        /// </summary>
        /// <param name="idClient"></param>
        /// <param name="idContact"></param>
        /// <param name="client"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("api/v{version:apiVersion}/client/{idClient}/contact/{idContact}")]
        [ResponseType(typeof(GenericResponse<string>))]
        public IHttpActionResult PutContact(int idClient, int idContact, ContactBase client)
        {
            return Ok();
        }

        /// <summary>
        /// Delete Contat for specific Client
        /// </summary>
        /// <param name="idClient"></param>
        /// <param name="idContact"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/v{version:apiVersion}/client/{idClient}/contact/{idContact}")]
        [ResponseType(typeof(GenericResponse<string>))]
        public IHttpActionResult DeleteContact(int idClient, int idContact)
        {
            return Ok();
        }
        #endregion
    }
}
