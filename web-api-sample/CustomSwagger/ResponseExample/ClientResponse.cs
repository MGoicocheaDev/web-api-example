﻿using Swashbuckle.Examples;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using web_api_sample.Models;

namespace web_api_sample.CustomSwagger.ResponseExample
{
    /// <summary>
    /// Client Specific Example
    /// </summary>
    public class ClientResponseExamples : IExamplesProvider
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public object GetExamples()
        {
            return new GenericResponse<Client>
            {
                Data = new Client
                {
                    ClientId = 1,
                    CompanyNumber = "20437898289",
                    Name = "Company Name SL",
                    Size = SizeCompany.small,
                    Contacts = new List<Contact>
                    {
                        new Contact
                        {
                            IdContact = 2,
                            DocumentNumber = "45678557",
                            TypeDocument = "CE",
                            Email = "contact@email.com",
                            Phone = "987644633",
                            FirstName = "First Name",
                            LastName = "Last Name"
                        }
                    }
                },
                ErrorManager = new ErrorManager
                {
                    Status = 200,
                    Errors = new List<DetailError>
                    {
                        new DetailError
                        {
                            ErrorNumber = 0,
                            Description = "Exito"
                        }
                    }
                }
            };
        }
    }

    /// <summary>
    /// Client Specific Example
    /// </summary>
    public class ClientResponseErrorExamples : IExamplesProvider
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public object GetExamples()
        {
            return new GenericResponse<Client>
            {
                Data = null,
                ErrorManager = new ErrorManager
                {
                    Status = 400,
                    Errors = new List<DetailError>
                    {
                        new DetailError
                        {
                            ErrorNumber= 1,
                            Description = "Campos invalidos"
                        }
                    }
                }
            };
        }
    }
    
}