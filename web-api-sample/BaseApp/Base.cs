﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_api_sample.BaseApp
{
    /// <summary>
    /// Clase solo de uso exclusivo para el ejemplo
    /// </summary>
    public static class Base
    {
        /// <summary>
        /// return list RUC
        /// </summary>
        /// <returns></returns>
        public static List<string> GetListRuc()
        {
            return new List<string> {
                "10203020492",
                "20332434223",
                "10286437643",
                "10973840023",
                "20958608400",
                "20348585439",
                "10383458435",
                "10203020443",
                "10203020342",
                "10203020242",
                "10203020492",
                "10203424234",
                "10203087670",
                "10203098700",
                "10203578765",
                "20584840492",
                "20378648954"
            };
        }

        /// <summary>
        /// list type documents
        /// </summary>
        /// <returns></returns>
        public static List<string> GetListTypeDocumentsContact()
        {
            return new List<string> { "DNI", "CE" };
        }
    }
}